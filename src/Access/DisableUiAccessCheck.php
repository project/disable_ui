<?php

namespace Drupal\disable_ui\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Checks whether the current user has permission to access non-API routes.
 */
class DisableUiAccessCheck implements AccessInterface {

  /**
   * The name of the permission users must have to access non-API routes.
   */
  protected const ACCESS_PERMISSION = 'access ui route';

  /**
   * Checks access.
   */
  public function access(AccountInterface $account): AccessResultInterface {
    return AccessResult::allowedIfHasPermission($account, static::ACCESS_PERMISSION);
  }

}
