<?php

namespace Drupal\disable_ui\Routing;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * A route subscriber to restrict all HTML routes on the site except log-in.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The regular expression used to detect API routes by format requirements.
   *
   * This expression should detect any route that declares a `_format`
   * requirement starting with the string `api_` or ending with 'json'.
   */
  protected const API_ROUTE_FORMAT_REGEX = '/(?:^api_.+$)|(?:^.*json$)/';

  /**
   * The Drupal module handler service (used for invoking hooks).
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -1000];

    return $events;
  }

  /**
   * Constructor for RouteSubscriber.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The Drupal module handler service (used for invoking hooks).
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): RouteCollection {
    foreach ($collection as $route_name => $route) {
      if ($this->isApiRoute($route) || $this->isExemptRoute($route_name)) {
        continue;
      }

      $route->setRequirement('_disable_ui', 'TRUE');
    }

    return $collection;
  }

  /**
   * Determines whether the given route is an API (non-HTML) route.
   *
   * The determination is based on the format requirements specified on the
   * route. Any route that declares a `_format` requirement that matches the
   * API_ROUTE_FORMAT_REGEX regular expression above. This crude heuristic
   * seems to work for both the RESTful Web Services and JSON:API modules in
   * core. YMMV for third-party modules that handle API requests in their own
   * controllers without any special sentinel values in their routes.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check.
   *
   * @return bool
   *   TRUE if the given route is considered a non-API (HTML) route.
   */
  protected function isApiRoute(Route $route): bool {
    if (!$route->hasRequirement('_format')) {
      return FALSE;
    }

    // Account for routes with multiple formats.
    $route_format = $route->getRequirement('_format');
    if (strpos($route_format, '|') !== FALSE) {
      $formats = explode('|', $route_format);
    }
    else {
      $formats = [$route_format];
    }

    foreach ($formats as $format) {
      if (preg_match(self::API_ROUTE_FORMAT_REGEX, $format)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Determines if the given route is considered exempt from restrictions.
   *
   * Modules influence the result of this method by implementing
   * hook_disable_ui_route_exclusions().
   *
   * @param string $route_name
   *   The machine name of the route.
   *
   * @return bool
   *   TRUE if the route is exempt from restrictions.
   *
   * @see hook_disable_ui_route_exclusions()
   */
  protected function isExemptRoute(string $route_name): bool {
    $exempt_routes = drupal_static(__METHOD__);

    if ($exempt_routes === NULL) {
      $exempt_routes = $this->moduleHandler->invokeAll('disable_ui_route_exclusions');
    }

    return in_array($route_name, $exempt_routes, TRUE);
  }

}
