# Disable UI
This small module is intended to be used on headless Drupal sites where only 
admins and developers should have access to the Drupal UI, and 
regular/authenticated users should not. This is accomplished via a new 
permission called "Access UI routes".

The permission only applies to routes that the module determines are not API
routes. The determination is based on the format requirements specified on each
route. Any route that declares a `_format` requirement that starts with the 
string `'api_'` or ends with the string `'json'` is considered an API route. 
This crude heuristic seems to work for both the RESTful Web Services and 
JSON:API modules in core. YMMV for third-party modules that handle API requests
in their own controllers without any special sentinel values in their routes.

In addition, special system routes (like the CSRF token endpoint for RESTful Web
Services) as well as the user login and password reset forms are excluded from
access restrictions via a new `hook_disable_ui_route_exclusions()`. If your site
needs to expose more routes to users, you should either ensure that the
`_format` of your route is correct (if you are exposing an API) or you should
implement the new hook (if you are exposing an HTML page).
