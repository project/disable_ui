<?php

/**
 * @file
 * Disable UI API/hook documentation.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Returns the list of menu routes that are exempt from Disable UI restrictions.
 *
 * This should be used for special, critical pages that all users should have
 * access to, such as the user log-in and password reset pages (so admins can
 * access the site) and the REST CSRF token path.
 *
 * If you are exposing a third-party API, instead of implementing this hook, it
 * is recommended that you indicate a route is an API route by providing a
 * `_format` requirement on the route, with a format starting with "api_". For
 * example, "api_json".
 *
 * @return string[]
 *   The array of route names to exclude from Disable UI restrictions.
 */
function hook_disable_ui_route_exclusions(): array {
  return [
    'user.login',
    'user.logout',
    'user.pass',
    'user.reset',
    'user.reset.form',
    'user.reset.login',
    'system.csrftoken',

    // NOTE: This route is deprecated in Drupal 9.x.
    'rest.csrftoken',
  ];
}

/**
 * @} End of "addtogroup hooks".
 */
